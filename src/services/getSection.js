import axios from "axios";
const apikey = process.env.REACT_APP_APIKEY;

export const getSection = () => {
  return axios
    .get(`section-list.json?api-key=${apikey}`)
    .then((res) => {
      console.log("res from /getSections", res.data.results);
      return { hasError: false, sections: res.data.results };
    })
    .catch((err) => {
      console.log("error from /getSection", err.response.data);
      return { hasError: true, message: err.response.data };
    });
};
