import axios from "axios";
const apikey = process.env.REACT_APP_APIKEY;

export const getContent = () => {
  return axios
    .get(`all/all.json?api-key=${apikey}`)
    .then((res) => {
      console.log("res from /getContent", res.data.results);
      return { hasError: false, contents: res.data.results };
    })
    .catch((err) => {
      console.log("error from /getContent", err.response.data);
      return { hasError: true, message: err.response.data };
    });
};
