import React from "react";
import { Button, Card, Col, Jumbotron, Row } from "react-bootstrap";

export const Content = (props) => {
  return (
    <Col md={{ offset: 3, span: 9 }}>
      {props.contents.length ? (
        <Row md={3}>
          {props.contents.map((content, i) => (
            <ContentCard key={i} {...content} />
          ))}
        </Row>
      ) : (
        <Jumbotron className="text-center">
          <h1>No Articles to show</h1>
        </Jumbotron>
      )}
    </Col>
  );
};

export const ContentCard = (props) => {
  const handleReadLater = () => {
    const laterArticles = JSON.parse(sessionStorage.getItem("readLater")) || [];
    const article = laterArticles.find(
      (article) => article.title === props.title
    );
    if (!article) {
      laterArticles.push({
        title: props.title,
        url: props.url,
        abstract: props.abstract,
        readLater: true,
      });
      sessionStorage.setItem("readLater", JSON.stringify(laterArticles));
    }
  };

  return (
    <Col>
      <Card className="mt-2">
        {props.thumbnail_standard && (
          <Card.Img
            height="175px"
            variant="top"
            src={props.thumbnail_standard}
          />
        )}
        <Card.Body>
          <Card.Title>{props.title}</Card.Title>
          <Card.Text>
            <a href={props.url} rel="noreferrer" target="_blank">
              {props.abstract}
            </a>
          </Card.Text>
          {props.readLater ? (
            <Button
              variant="danger"
              onClick={() => props.remove({ title: props.title })}
            >
              Remove
            </Button>
          ) : (
            <Button onClick={handleReadLater}>Read Later</Button>
          )}
        </Card.Body>
      </Card>
    </Col>
  );
};
