import React from "react";
import { Navbar, NavDropdown } from "react-bootstrap";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { removeUserAction } from "../actions/userActions";

const NavBar = (props) => {
  const history = useHistory();
  const { user, removeUser } = props;

  const handleLogout = () => {
    removeUser();
    history.push("/login");
  };
  return (
    <Navbar bg="dark" variant="dark">
      <Link to={{ pathname: "/", state: user }}>
        <Navbar.Brand>News App</Navbar.Brand>
      </Link>
      {user && (
        <NavDropdown
          className="ml-auto text-capitalize"
          title={`Hi, ${user.displayName}`}
        >
          <NavDropdown.Item className="text-decoration-none">
            <Link
              className="text-decoration-none text-dark"
              to={{ pathname: "/editProfile", state: user }}
            >
              Edit Profile
            </Link>
          </NavDropdown.Item>
          <NavDropdown.Item>
            <Link
              className="text-decoration-none text-dark"
              to={{ pathname: "/readLater", state: user }}
            >
              Read Later
            </Link>
          </NavDropdown.Item>
          <NavDropdown.Item onClick={handleLogout}>Logout</NavDropdown.Item>
        </NavDropdown>
      )}
    </Navbar>
  );
};

const mapStateToProps = (state) => {
  debugger;
  return { user: state.user };
};
const mapDispatchToProps = (dispatch) => {
  return { removeUser: () => dispatch(removeUserAction()) };
};
export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
