import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";

export const Signup = (props) => {
  const history = useHistory();
  const users = JSON.parse(localStorage.getItem("users")) || [];
  const initialValues = {
    email: "",
    displayName: "",
    password: "",
  };

  const [signupData, setSignupData] = useState(initialValues);
  const [errors, setErrors] = useState(initialValues);

  const handleChange = (e) => {
    const temp = errors;
    if (e.target.name !== "email") {
      if (/[^a-zA-Z0-9-]/g.test(e.target.value)) {
        temp[e.target.name] = "Special Characters are not allowed";
      } else temp[e.target.name] = "";
    }
    setSignupData({ ...signupData, [e.target.name]: e.target.value });
    setErrors(temp);
  };

  const validate = () => {
    return Object.values(errors).every((e) => e === "");
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      const user = users.find((user) => user.email === signupData.email);
      if (!user) {
        users.push(signupData);
        localStorage.setItem("users", JSON.stringify(users));
        window.alert("User Added Successfully!");
        setSignupData(initialValues);
        history.push("/login");
      } else {
        setErrors({ ...errors, email: "Email already exists" });
      }
    }
  };

  return (
    <Modal.Dialog>
      <Modal.Header>
        <Modal.Title>SignUp</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              name="email"
              placeholder="Enter Email"
              value={signupData.email}
              onChange={handleChange}
              required
            />
            <Form.Text className="text-danger">{errors.email}</Form.Text>
          </Form.Group>

          <Form.Group>
            <Form.Label>Display Name</Form.Label>
            <Form.Control
              type="text"
              name="displayName"
              placeholder="Enter Display Name"
              value={signupData.displayName}
              onChange={handleChange}
              required
            />
            <Form.Text className="text-danger">{errors.displayName}</Form.Text>
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              placeholder="Enter Password"
              value={signupData.password}
              onChange={handleChange}
              maxLength={20}
              required
            />
            <Form.Text className="text-danger">{errors.password}</Form.Text>
          </Form.Group>
          <Form.Text>
            Already have an Account? <Link to="/login">Login</Link>
          </Form.Text>
          <Button type="submit" className="mt-1">
            SignUp
          </Button>
        </Form>
      </Modal.Body>
    </Modal.Dialog>
  );
};
