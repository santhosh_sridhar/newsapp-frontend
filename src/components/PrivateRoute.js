import React from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router";
import NavBar from "./NavBar";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { user } = rest;
  return (
    <Route
      {...rest}
      exact
      render={(props) =>
        Object.keys(user).length ? (
          <>
            <NavBar />
            <Component {...props} />
          </>
        ) : (
          <Redirect to="/login" />
        )
      }
    />
  );
};

const mapStateToProps = (state) => {
  return { user: state.user };
};

export default connect(mapStateToProps)(PrivateRoute);
