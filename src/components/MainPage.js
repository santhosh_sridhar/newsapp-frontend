import React, { useEffect, useState } from "react";
import { Container, Row } from "react-bootstrap";
import { getContent } from "../services/getContent";
import { getSection } from "../services/getSection";
import { Content } from "./Content";
import { Section } from "./Section";

export const MainPage = (props) => {
  const [sections, setSections] = useState([]);
  const [contents, setContents] = useState([]);
  const [selectedContents, setSelectedContent] = useState([]);
  useEffect(() => {
    getSection().then((res) => {
      if (!res.hasError) setSections([...res.sections]);
    });
    getContent().then((res) => {
      if (!res.hasError) setContents([...res.contents]);
    });
  }, []);

  const onSectionClick = (section) => {
    console.log("selected section", section);
    const temp = contents.filter(
      (content) => content.section === section.display_name
    );
    console.log("selected contents", temp);
    setSelectedContent([...temp]);
  };

  return (
    <>
      <Container fluid>
        <Row>
          <Section sections={sections} onSectionClick={onSectionClick} />
          <Content contents={selectedContents} />
        </Row>
      </Container>
    </>
  );
};
