import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { updateUserAction } from "../actions/userActions";

const Login = (props) => {
  const { updateUser } = props;
  const history = useHistory();
  const initialValues = { email: "", password: "" };
  const [loginData, setLoginData] = useState(initialValues);
  const [errors, setErrors] = useState(initialValues);
  const users = JSON.parse(localStorage.getItem("users")) || [];
  const handleChange = (e) => {
    setLoginData({ ...loginData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const temp = initialValues;
    const user = users.find((user) => user.email === loginData.email);
    if (user) {
      if (user.password !== loginData.password)
        temp.password = "Invalid Password";
      else {
        updateUser(user);
        history.push("/");
      }
    } else {
      temp.email = "Email doesn't exist";
    }
    setErrors(temp);
  };
  return (
    <Modal.Dialog>
      <Modal.Header>
        <Modal.Title>Login</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              name="email"
              placeholder="Enter Email"
              onChange={handleChange}
              required
            />
            <Form.Text className="text-danger">{errors.email}</Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              max={20}
              name="password"
              placeholder="Enter Email"
              onChange={handleChange}
              required
            />
            <Form.Text className="text-danger">{errors.password}</Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Text>
              Don't have an account? <Link to="/signup">SignUp</Link>
            </Form.Text>
          </Form.Group>
          <Button className="mt-2" type="submit">
            Login
          </Button>
        </Form>
      </Modal.Body>
    </Modal.Dialog>
  );
};

const mapDispatchToProps = (dispatch) => {
  return { updateUser: (user) => dispatch(updateUserAction(user)) };
};

export default connect(null, mapDispatchToProps)(Login);
