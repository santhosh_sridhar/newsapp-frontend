import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Jumbotron, Row } from "react-bootstrap";
import { ContentCard } from "./Content";

export const ReadLater = (props) => {
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    const readLater = JSON.parse(sessionStorage.getItem("readLater"));
    if (readLater) setArticles([...readLater]);
  }, []);

  const handleRemoveArticle = (article) => {
    const updatedArticles = articles.filter((a) => a.title !== article.title);
    setArticles([...updatedArticles]);
    sessionStorage.setItem("readLater", JSON.stringify(updatedArticles));
  };
  if (articles.length)
    return (
      <Row md={3}>
        {articles.map((article) => (
          <ContentCard {...article} remove={handleRemoveArticle} />
        ))}
      </Row>
    );
  else
    return (
      <Jumbotron className="text-center">
        <h1>No Articles to Read!</h1>
      </Jumbotron>
    );
};
