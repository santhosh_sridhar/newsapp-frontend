import React from "react";
import { Col, ListGroup } from "react-bootstrap";

export const Section = (props) => {
  const { sections, onSectionClick } = props;
  return (
    <Col
      className="bg-light p-0 w-25 h-100 position-fixed "
      style={{ overflow: "auto" }}
    >
      <ListGroup className="w-100">
        <ListGroup.Item variant="info">
          <h5>Section</h5>
        </ListGroup.Item>
        {sections.map((section, i) => (
          <ListGroup.Item
            key={i}
            action
            variant="dark"
            onClick={() => onSectionClick(section)}
          >
            {section.display_name}
          </ListGroup.Item>
        ))}
      </ListGroup>
    </Col>
  );
};
