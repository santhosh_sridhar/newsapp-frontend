import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { updateUserAction } from "../actions/userActions";
import { useHistory } from "react-router-dom";

const EditUserProfile = (props) => {
  const history = useHistory();
  const initialValues = {
    email: "",
    displayName: "",
    password: "",
  };
  const [user, setUser] = useState({});
  const [errors, setErrors] = useState(initialValues);
  useEffect(() => {
    setUser(props.user);
  }, [props.user]);

  const handleChange = (e) => {
    const temp = errors;
    if (/[^a-zA-Z0-9-]/g.test(e.target.value)) {
      temp[e.target.name] = "Special Characters are not allowed";
    } else temp[e.target.name] = "";
    setUser({ ...user, [e.target.name]: e.target.value });
    setErrors(temp);
  };

  const validate = () => {
    return Object.values(errors).every((e) => e === "");
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      const users = JSON.parse(localStorage.getItem("users"));
      const index = users.findIndex((u) => u.email === user.email);
      users[index] = user;
      console.log("updated", users);
      props.updateUser(user);
      window.alert("User updated successfully");
      history.push("/", user);
      localStorage.setItem("users", JSON.stringify(users));
    }
  };
  return (
    <Modal.Dialog>
      <Modal.Header>
        <Modal.Title>Edit User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              name="email"
              placeholder="Enter Email"
              value={user.email}
              disabled
              onChange={handleChange}
              required
            />
            <Form.Text className="text-danger">{errors.email}</Form.Text>
          </Form.Group>

          <Form.Group>
            <Form.Label>Display Name</Form.Label>
            <Form.Control
              type="text"
              name="displayName"
              placeholder="Enter Display Name"
              value={user.displayName}
              onChange={handleChange}
              required
            />
            <Form.Text className="text-danger">{errors.displayName}</Form.Text>
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              placeholder="Enter Password"
              value={user.password}
              onChange={handleChange}
              maxLength={20}
              required
            />
            <Form.Text className="text-danger">{errors.password}</Form.Text>
          </Form.Group>

          <Button type="submit" className="mt-1">
            Edit
          </Button>
        </Form>
      </Modal.Body>
    </Modal.Dialog>
  );
};

const mapStateToProps = (state) => {
  return { user: state.user };
};

const mapDispatchToProps = (dispatch) => {
  return { updateUser: (user) => dispatch(updateUserAction(user)) };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditUserProfile);
