export const updateUserAction = (user) => {
  return { type: "UPDATE", user };
};

export const removeUserAction = () => {
  return { type: "REMOVE" };
};
