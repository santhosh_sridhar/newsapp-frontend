export const userReducer = (state = {}, action) => {
  switch (action.type) {
    case "UPDATE":
      state = { ...action.user };
      break;
    case "REMOVE":
      state = {};
      break;
  }
  return state;
};
