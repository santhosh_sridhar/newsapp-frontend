import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router";
import { Jumbotron } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Login from "./components/Login";
import { Signup } from "./components/Signup";
import { MainPage } from "./components/MainPage";
import { ReadLater } from "./components/ReadLater";
import PrivateRoute from "./components/PrivateRoute";
import EditUserProfile from "./components/EditUserProfile";
import { Provider } from "react-redux";
import { createStore } from "redux";
// import { NavBar } from "./components/NavBar";
import reducer from "./reducer";

const store = createStore(reducer);

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/login" component={Login} exact />
            <Route path="/signup" component={Signup} exact />
            <PrivateRoute path="/readLater" component={ReadLater} exact />
            <PrivateRoute path="/" component={MainPage} exact />
            <PrivateRoute
              path="/editProfile"
              component={EditUserProfile}
              exact
            />
            <Route
              path="*"
              render={() => (
                <Jumbotron className="mt-5 bg-light">
                  <h1>404 Page Not Found</h1>
                </Jumbotron>
              )}
            />
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
